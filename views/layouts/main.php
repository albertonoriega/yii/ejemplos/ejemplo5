<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Array', 'items' => [
                ['label' => 'Ejercicio1', 'url' => ['/site/ejercicio1']],
                ['label' => 'Ejercicio2', 'url' => ['/site/ejercicio2']],
                ['label' => 'Ejercicio3', 'url' => ['/site/ejercicio3']],
                ['label' => 'Ejercicio 4', 'url' => ['/site/ejercicio4']],
                ['label' => 'Ejercicio 5', 'url' => ['/site/ejercicio5']],
                ['label' => 'Ejercicio 6', 'url' => ['/site/ejercicio6']],
        ]],
            ['label' => 'Modelos ActiveRecord', 'items' => [
                ['label' => 'Ejercicio7', 'url' => ['/site/ejercicio7']],
                ['label' => 'Ejercicio8', 'url' => ['/site/ejercicio8']],
        ]],
            ['label' => 'Mostrar un registro', 'items' => [
                ['label' => 'Ejercicio9', 'url' => ['/site/ejercicio9']],
                ['label' => 'Ejercicio10', 'url' => ['/site/ejercicio10']],
                ['label' => 'Ejercicio11', 'url' => ['/site/ejercicio11']],
                ['label' => 'Ejercicio12', 'url' => ['/site/ejercicio12']],
        ]],
            ['label' => 'Ejercicio13', 'url' => ['/site/ejercicio13']],
    ]]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; My Company <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
