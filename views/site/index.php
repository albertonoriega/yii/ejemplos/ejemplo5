<?php

use yii\helpers\Html;
use yii\web\View;

/* @var $this View */

$this->title = 'Visualizacion de arrays y modelos con BBDD';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Ejemplo 5</h1>

        <p class="lead">Visualizacion de arrays y modelos con BBDD </p>


    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2>Ejercicio1</h2>
                <div>

                    Mostrar un array enumerado de arrays asociativos   
                    <ul>
                        <li>Para realizarlo, pasamos el array a la vista y utilizo un Html:: ul </li> 
                        <li>Creo una subvista para mostrar cada registro </li>
                        <li>En la subvista hacemos uso de la clase card de bootstrap para mostrar los registros </li>
                    </ul>
                </div>
                <p><?=
                    Html::a("Ejercicio 1", // label
                            ["site/ejercicio1"], // controlador/vista
                            ["class" => "btn btn-primary"]) // estilo visual del boton
                    ?></p>
            </div>
            
            
            <div class="col-lg-6">
                <h2>Ejercicio2</h2>
                <div>

                    Mostrar un array enumerado 
                    <ul>
                        <li>Para realizarlo, pasamos el array a la vista y utilizo un Html:: ul </li> 
                    
                </div>
                <p><?=
                    Html::a("Ejercicio 2", // label
                            ["site/ejercicio2"], // controlador/vista
                            ["class" => "btn btn-primary"]) // estilo visual del boton
                    ?></p>
            </div>
                
            
            <div class="col-lg-6">
                <h2>Ejercicio3</h2>
                <div>

                    Mostrar dos arrays enumerados de arrays asociativos   
                    <ul>
                        <li>Para realizarlo, pasamos los dos arrays a la vista</li> 
                        <li>Para mostrar cada array empleamos Html:: ul</li>
                        <li>Creo una subvista para mostrar cada registro </li>
                        <li>En la subvista hacemos uso de la clase card de bootstrap para mostrar los registros </li>
                    </ul>
                </div>
                <p><?=
                    Html::a("Ejercicio 3", // label
                            ["site/ejercicio3"], // controlador/vista
                            ["class" => "btn btn-primary"]) // estilo visual del boton
                    ?></p>
            </div>    
            
            <div class="col-lg-6">
                <h2>Ejercicio4</h2>
                <div>
                    
                    Mostrar un array enumerado de arrays asociativos
                    <ul>
                        <li>Convertimos el array en un DataProvider con ArrayDataProvider </li> 
                        <li>Se lo mandamos a la vista y lo mostramos con un GRIDVIEW </li>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 4", // label
                        ["site/ejercicio4"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio5</h2>
                <div>
                    
                    Mostrar un array de modelos con un GRIDVIEW
                    <ul>
                        <li>Creamos un modelo de alumnos donde insertamos los atributos, las reglas, y los attribute labels </li>
                        <li>Generamos un array de modelos </li>
                        <li>Convertimos el array en un DataProvider con ArrayDataProvider </li> 
                        <li>Se lo mandamos a la vista y lo mostramos con un GRIDVIEW </li>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 5", // label
                        ["site/ejercicio5"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio6</h2>
                <div>
                    
                    Mostrar un array de modelos con un LISTVIEW
                    <ul>
                        <li>Creamos un modelo de alumnos donde insertamos los atributos, las reglas, y los attribute labels </li>
                        <li>Generamos un array de modelos </li>
                        <li>Convertimos el array en un DataProvider con ArrayDataProvider </li> 
                        <li>Se lo mandamos a la vista y lo mostramos con un LISTVIEW </li>
                        <li>Al ser LISTVIEW necesitamos una subvista para mostrar cada campo del array </li>
                    </ul>
                </div>
                <p><?= Html::a("Ejercicio 6", // label
                        ["site/ejercicio6"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio7</h2>
                <div>
                    
                    Mostrar la tabla alumno a través de un GRIDVIEW
                    <ul>
                        <li>Generamos un modelo de la tabla Alumno de la BBDD con Gii</li>
                        <li>Creamos un active query de Alumno</li>
                        <li>Creamos un dataProvider del activeQuery con ActiveDataProvider</li>
                        <li>Lo mostramos en la vista con un GRIDVIEW</li>
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 7", // label
                        ["site/ejercicio7"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio8</h2>
                <div>
                    
                    Mostrar la tabla alumno a través de un GRIDVIEW
                    <ul>
                        <li>Creamos un active query de Alumno</li>
                        <li>Ejecutamos la consulta para que nos devuelva un array de modelos</li>
                        <li>Convertimos el array en un DataProvider con ArrayDataProvider </li> 
                        <li>Lo mostramos en la vista con un GRIDVIEW</li>
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 8", // label
                        ["site/ejercicio8"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio9</h2>
                <div>
                    
                    Mostrar un registro del modelo Alumnos usando DETAILVIEW
                    <ul>
                        <li>Creamos un array de modelos de Alumnos e introducimos los valores de cada atributo</li>
                        <li>Mandamos el array de modelos a la vista</li>
                        <li>Lo mostramos en la vista con un DETAIL VIEW</li>
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 9", // label
                        ["site/ejercicio9"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio10</h2>
                <div>
                    
                    Mostrar un registro de un array asociativo usando DETAILVIEW
                    <ul>
                        <li>Generamos el array asociativo e introducimos los valores de cada campo</li>
                        <li>Mandamos el array a la vista</li>
                        <li>Lo mostramos en la vista con un DETAIL VIEW</li>
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 10", // label
                        ["site/ejercicio10"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio11</h2>
                <div>
                    
                    Insertar o actualizar un registro en la BBDD
                    <ul>
                        <li>Generamos un modelo de Alumno e introducimos los valores de cada campo</li>
                        <li>Guardamos o insertamos el registro en la BBDD con save()</li>
                        <li>Mandamos modelo a la vista</li>
                        <li>Lo mostramos en la vista con un DETAIL VIEW</li>
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 11", // label
                        ["site/ejercicio11"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio12</h2>
                <div>
                    
                    Extraer y mostrar un registro en la BBDD
                    <ul>
                        <li>Extraemos de la tabla Alumno de la BBDD un registro con la funcion findOne()</li>
                        <li>Mandamos el modelo a la vista</li>
                        <li>Lo mostramos en la vista con un DETAIL VIEW</li>
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 12", // label
                        ["site/ejercicio12"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio13</h2>
                <div>
                    
                    Listar directorio
                    <ul>
                        <li>Generamos un array con el contenido del directorio actual usando la funcion scandir</li>
                        <li>Mandamos el array a la vista</li>
                            <ul>
                                <li>NOTA: No podemos pasar el array con un DATA PROVIDER porque tenemos un array enumerado, no tenemos un array asociativo</li>
                            </ul>
                        <li>Lo mostramos en la vista con un Html:: ul y con un foreach</li>
                    </ul>
                                     
                    
                </div>
                <p><?= Html::a("Ejercicio 13", // label
                        ["site/ejercicio13"],  // controlador/vista
                        ["class"=> "btn btn-primary"]) // estilo visual del boton
                ?></p>
            </div>
            
            
            
        </div>
    </div>