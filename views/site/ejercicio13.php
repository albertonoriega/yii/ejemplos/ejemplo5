<?php

// con el helper UL


use yii\grid\GridView;
use yii\helpers\Html;

echo "CON UL";
echo Html::ul($contenido);


// si lo hacemos con un foreach
echo "<br> CON UN FOREACH";

foreach ($contenido as $value) {
    ?>
<ul>
    <?php
    echo "<li> " . $value . "</li>";
    ?>
    </ul>
<?php
}

// Con el data provider no sale nada porque el array es enumerado
// Cuando tenemos un array enumerado solo podemos mostarlo con el UL o el foreach
// con el data provider no muestra nada porque los indices no tienen nombre
echo "<br> CON EL DATA PROVIDER";

echo GridView::widget([
   "dataProvider" => $dataProvider, 
]);

