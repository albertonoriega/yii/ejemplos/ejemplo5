<?php

namespace app\controllers;

use app\models\Alumno;
use app\models\Alumnos;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use const YII_ENV_TEST;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionEjercicio1(){
        $alumnos=[
          [
              "id"=> 1,
              "nombre"=>"Eva",
              "curso"=> "Excel",
          ],
          [
              "id"=> 2,
              "nombre"=>"Luis",
              "curso"=> "Access", 
          ],
          [
              "id"=> 3,
              "nombre"=>"Susana",
              "curso"=> "Excel",
          ],
        ];
        
        
         return $this->render('ejercicio1', [
             "datos"=>$alumnos,
             ]);
        
    }
    
    public function actionEjercicio2(){
        $numeros=[1,2,3,4,5,6,7,8,9];
        return $this->render('ejercicio2',[
            "numeros"=>$numeros
    ]);
        
    
    }
    
    
    public function actionEjercicio3(){
        $aprobados=[
          [
              "id"=> 1,
              "nombre"=>"Eva",
              "curso"=> "Excel",
          ],
          [
              "id"=> 2,
              "nombre"=>"Luis",
              "curso"=> "Access", 
          ],
            ];
        $suspensos=[["id"=> 3,
              "nombre"=>"Susana",
              "curso"=> "Excel",
          ],
        ];
            return $this->render('ejercicio3',[
            "aprobados"=> $aprobados,
            "suspensos"=> $suspensos ,  
    ]);
        
    }
    
    public function actionEjercicio4(){
         // Lo mismo que en el ejercicio 1 pero lo visualizamos con un GridView
        // Array de arrays asociativos
        $alumnos=[
          [
              "id"=> 1,
              "nombre"=>"Eva",
              "curso"=> "Excel",
          ],
          [
              "id"=> 2,
              "nombre"=>"Luis",
              "curso"=> "Access", 
          ],
          [
              "id"=> 3,
              "nombre"=>"Susana",
              "curso"=> "Excel",
          ],
        ];

        
        // aqui muestro los alumnos con un grid view
        $dataProvider= new ArrayDataProvider([
            'allModels'=>$alumnos,
        ]);
               
        return $this->render('ejercicio4',[
            "dataProvider" => $dataProvider,
        ]);
        
    }
    
    public function actionEjercicio5(){
        // Hacemos lo mismo que en el ejercicio4 pero con un array de modelos
        // La ventaja de los modelos es que si tu cambias un dato de un modelo se ve reflejado también en la base de datos
        // Array de modelos
        $alumnos = [
            new Alumnos(
                    [
                "id" => 1,
                "nombre" => "Eva",
                "curso" => "Excel",
                    ]),
            new Alumnos(
                    [
                "id" => 2,
                "nombre" => "Luis",
                "curso" => "Access"
                    ]),
            new Alumnos(
                    [
                "id" => 3,
                "nombre" => "Susana",
                "curso" => "Excel"
                    ]),
        ];
        
        // Creacion del data provider Grid view
        $dataProvider= new ArrayDataProvider([
            'allModels'=>$alumnos,
        ]);
        
        //redenrización del data provider
        return $this->render('ejercicio4',[
            "dataProvider" => $dataProvider,
        ]);
        
    }
    
    public function actionEjercicio6(){
        
        $alumnos = [
            new Alumnos(
                    [
                "id" => 1,
                "nombre" => "Eva",
                "curso" => "Excel",
                    ]),
            new Alumnos(
                    [
                "id" => 2,
                "nombre" => "Luis",
                "curso" => "Access"
                    ]),
            new Alumnos(
                    [
                "id" => 3,
                "nombre" => "Susana",
                "curso" => "Excel"
                    ]),
        ];
        
        // Creacion del data provider con un list view
        $dataProvider= new ArrayDataProvider([
            'allModels'=>$alumnos,
        ]);
        
        //redenrización del data provider
        return $this->render('ejercicio6',[
            "dataProvider" => $dataProvider,
        ]);
        
    }
    
    public function actionEjercicio7(){
        // quiero consultar la tabla alumno
        
        // consulta sin ejecutar
        // Me devuelve un ActiveQuery
        $query = Alumno::find(); 
        
        $dataProvider=new ActiveDataProvider([
            "query" => $query,
        ]);
        
        // Mandarle el resultado de la query a una vista
        return $this->render("ejercicio7",[
            "dataProvider"=> $dataProvider,
        ]);
        
    
        
    }
    
    public function actionEjercicio8(){
        // quiero consultar la tabla alumno
        
        // consulta ejecutada
        // me devuelve un array de ActiveRecord (modelos)
        $resultado = Alumno::find()->all(); 
            
        $dataProvider= new ArrayDataProvider([
                "allModels" => $resultado,
        ]);
        
        // Mandarle el resultado de la query a una vista
        return $this->render("ejercicio7",[
            "dataProvider"=> $dataProvider,
        ]);
        
    }
    
    public function actionEjercicio9(){
        // Creo un modelo llamado Alumnos
        $model = new Alumnos();
        $model->id=10;
        $model->nombre="Federico";
        $model->curso="Word";
        
        // mando el modelo a la vista, y en la vista los visualizo con un detail view
        return $this->render("ejercicio9",[
            "model"=> $model,
        ]);
    }
    
    public function actionEjercicio10(){

        // Creo un registro con un  array asociativo
        $dato=[
          "id" => 10,
          "nombre" => "Federico",
          "curso" => "Word",
        ];
        
        // Visualizo el array asociativo en el DetailView del ejercicio anterior
        return $this->render("ejercicio9",[
            "model"=> $dato,
        ]);
        
    }
    
    public function actionEjercicio11 (){
        // Creando una clase basado en ActiveRecord
        $model = new Alumno();
        $model->id=10;
        $model->nombre="Federico";
        $model->curso="Word";
        $model->save(); // Inserto o actualizo el registro en la tabla
        
        return $this->render("ejercicio9",[
            "model"=> $model,
        ]);
        
        
    }
    
    public function actionEjercicio12 ($id=1){
        // Extraer un registro de la tabla SELECT * FROM alumno WHERE id=1
        $model = Alumno::findOne($id);
        
        
        
        return $this->render("ejercicio9",[
            "model"=> $model,
        ]);
              
    }
    
    //Listar directorio
    public function actionEjercicio13 (){
        // Array con el contenido del directorio actual
        $contenido = scandir(".");
        
        $dataProvider= new ArrayDataProvider([
            "allModels"=> $contenido,
        ]);
        
        return $this->render("ejercicio13",[
            "contenido" => $contenido,
            "dataProvider" => $dataProvider,
        ]);
    }
}
   
